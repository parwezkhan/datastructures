/*

given an array of elements firnd smallest element having exactly k repeatations


time complexity  O(n)
space complexity O(n)


*/
#include<iostream>
#include<unordered_map>
using namespace std;
int main()
{
	
int n,i;
 cout<<" enter no. of elements "<<endl;
 cin>>n;
 cout<<"  enter elements  "<<endl;
  int a[n];
  unordered_map<int,int>m;
  for( i=0;i<n;i++ )
  {
      cin>>a[i];
      m[ a[i] ]++;
  }
    int k;
    cout<<" enter no. of repeatations  you want "<<endl;
      cin>>k;

   int smallest= INT8_MAX;
   for(  auto it = m.begin(); it != m.end(); ++it  )
   	   {
           if(  it->second == k )  
                 {
                   smallest = min( smallest, it->first ) ;
                     
                 }
   	   }
      
      if( smallest == INT8_MAX )
      	  cout<<" no elements exits whose repeatations  is "<< k<<endl;
      else
          cout<< smallest<<endl;	
     return 0;
}
