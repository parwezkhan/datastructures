/*
   maximum of minimumof all size windows in an array

    time complexity O(n)
    space  O(n)


*/
#include <bits/stdc++.h>
using namespace std;
int main()
{
 int n;
 cin>>n;
 int a[n];
 for(  int i=0;i<n;i++)
    cin>>a[i];
 stack<int>s;
 int L[n+1],R[n+1],b[n+1];
        for(int i=0;i<=n;i++)
        {
            L[i]=-1;
            R[i]=n;
            b[i]=0;
        }

 for( int i= 0;i<n;i++ )  // previous smallest element index
 {
     while( ! s.empty()  && a[ s.top() ] >= a[i]   )
          s.pop();
     if( ! s.empty() )
         L[ i]= s.top();
     s.push(i);
 }
   while( ! s.empty() )
      s.pop();

   for( int i= n-1;i>=0;i-- )
   {
         while( ! s.empty()  && a[ s.top() ] >= a[i]   )
                 s.pop();
         if( ! s.empty() )
               R[ i]= s.top();
         s.push(i);
   }

     for(  int i=0;i<n;i++ )
     {
         int len= R[i] - L[i] -1;
         b[ len ]= max( b[ len ], a[i] );
     }
     for( int i= n-1;i>0;i-- )
     {
         b[i]= max( b[i],b[i+1] );

     }
      for( int i=1;i<=n;i++ )
        cout<<b[i]<<" ";

    return 0;
}
