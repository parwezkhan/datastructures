
/*
    max  j-i  for a[i] < a[j]

  O(n) time complexity
O(1)
  space
*/
    
#include <bits/stdc++.h>
using namespace std;
int main()
{
   int n;
   cin>>n;
   int a[n];
   for(int i=0;i<n;i++)
       cin>>a[i];
   int l[n],r[n];
   l[0]=a[0];
   for(int i=1;i<n;i++)
      l[i]= min(a[i],l[i-1] );
   r[n-1]=a[n-1];
   for( int i=n-2;i>=0;i--)
        r[i]= max( a[i],r[i+1] );
   int d = -1;
   int i=0,j=0;
   while( i<n && j<n )
   {
       if(  l[i] < r[j]  )
       {
           d = max( j - i ,d );
           j++;

       }
       else
           i++;
   }
     cout<< d<<endl;
    return 0;
}
