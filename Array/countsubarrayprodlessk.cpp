
/*

 given array of positive number find all subarray product < k


*/


// Program to find number of subarrays with product
// less than a given value K
#include <bits/stdc++.h>
using namespace std;
 
// Function that returns number of subarrays
int countSubArrayProductLessThanK(const vector<int>& arr,
                                                   int k)
{
    int start, end;
    int prod = 1;
    int count = 0;
    for (start = 0; start < arr.size(); start = end + 1) {
        for (end = start; end < arr.size(); end++) {
 
            prod = prod * arr[end];
 
            // If prod is more than k
            if (prod > k) {
 
                end--;
                int len = (end - start + 1);
 
                // the number of subarrays possible 
                // in a window is n(n+1)/2
                count += len * (len + 1) / 2;
 
                // restore the product
                prod = 1;
                break;
            } else if (end == arr.size() - 1) {
                int len = (end - start + 1); 
                count += (len) * (len + 1) / 2;
            }
        }
    }
    return count;
}
 
// Driver Function to count number of
// such arrays
int main()
{
    vector<int> A;
    A.push_back(1);
    A.push_back(2);
    A.push_back(3);
    A.push_back(4);
    int k = 10;
    cout << countSubArrayProductLessThanK(A, k);
}


