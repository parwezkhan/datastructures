/*
   next greater element for each elements of an array 
  if not exits print -1
o(n)
*/


#include <bits/stdc++.h>
using namespace std;
int main()
{
   int t;
   cin>>t;
   while( t-- )
   {
       int n;
       cin>>n;
       int a[n];
       for( int i=0;i<n;i++)
            cin>>a[i];
       int g[n];
       for(int i=0;i<n;i++)
           g[i]= -1;
         stack< int>s;
       for(  int i= n-1;i>=0;i--)
       {
            while( ! s.empty()  &&  s.top() < a[i] )
                   s.pop();
            if( !s.empty() &&  s.top() > a[i] )
                      g[i]= s.top();
            s.push(a[i]);
       }

       for(int i=0;i<n;i++)
           cout<< g[i]<<" ";
       cout<<endl;
   }
    return 0;
}
