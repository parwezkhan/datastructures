
#include <iostream>
#include<malloc.h>
using namespace std;
struct node
     {
        int data;
        struct node* left;
        struct node* right;
     };
   struct node *newnode( int data )
   {
      struct node *t= ( struct node * )malloc( sizeof(  struct node ) );
         t->data= data;
         t->left=t->right = NULL;
         return t;
   }
   class Stack
    {
      struct node* top = ( struct node * )malloc(  sizeof( struct node ) );
     public:
      void push( int e )
      {
          if( top == NULL )
          {
              top = newnode( e );
          }
          else
          {
              struct node *t = newnode( e );
                top->right = t;
                t->left= top;
                top= top->right ;
          }
      }
        void pop( )
         {
             if( top == NULL )
             {
                cout<<"  stack is empty ";
             }
             else
             {
                 top= top->left;
                 top->right= NULL;
             }

         }
       int Top()
        {
                if( top != NULL )
                return top->data ;

        }

    }  ;
int main()
{    Stack s;
     s.push( 10 );
     cout<< s.Top()<<endl;
     s.push(20);
     cout<< s.Top()<<endl;
     s.push(30);
     cout<< s.Top()<<endl;
      s.pop();
       cout<< s.Top()<<endl;
       s.pop();
        cout<< s.Top()<<endl;

    return 0;
}
