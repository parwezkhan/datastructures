// dishkhatra for single source to minimum distance to all vertx 
#include<bits/stdc++.h>
using namespace std;
#define INF LONG_MAX 
const int sz=10000; 
vector<pair<long,long> > g[sz]; 
 long dis[sz]; 
void Dijkstra(long source,long n) 
{   long i;
    bool vis[sz]={0}; 
    for( i=0L;i<sz;i++) 
        dis[i]=INF; 
    class prioritize{public: bool operator ()(pair<long,long>&p1 ,pair<long,long>&p2){return p1.second>p2.second;}};
    priority_queue<pair<long,long> ,vector<pair<long,long> >, prioritize> q; 
    q.push(make_pair(source,dis[source]=0L));
    while(!q.empty())
    {
        pair<long,long> curr=q.top(); 
        q.pop();
        long cv=curr.first,cw=curr.second; 
        if(vis[cv]) 
        continue;
        vis[cv]=true; 
        for( i=0;i<g[cv].size();i++)
            if(!vis[g[cv][i].first] && g[cv][i].second+cw<dis[g[cv][i].first]) 
                q.push(make_pair(g[cv][i].first,(dis[g[cv][i].first]=g[cv][i].second+cw))); 
    }
}
int main()
{
    int v,e,x,y,w,s;
  //  cout<<" enter no. of vertices and edges"<<endl;
    cin>>v>>e;
 //   cout<<" enter edges with weight   vertices  1 to "<<v<<endl;
    for(int i=0;i<e;i++) 
    {   
        cin>>x>>y>>w;
        g[x].push_back(make_pair(y,w));
        g[y].push_back(make_pair(x,w));
    }
 //   cout<<" enter source vertex "<<endl;
    cin>>s;
    Dijkstra(s,v);
    for(int i=1;i<=v;i++)
       cout<<dis[i]<<" ";
    return 0;
}


