/* shortest distance when there is a clique 
first k vertice have distance x to each other 1 to k
then m edges followed a b w
input 1 line contains no. of testcases
for each testcase n  k x m s
no. of vertices first k vertices x distance then m edges source vertex s 
to solve this take n+1 a fake vertex connect this to every 1 to k with distance x/2 
to deal with odd no.  double every distance so that x/2 integer 
in final distance divide by 2  */


#include<bits/stdc++.h>
using namespace std;
#define INF LLONG_MAX 
const long long sz=100003; 
vector<pair<long long,long long > > g[sz]; 
long long dis[sz]; 
void Dijkstra(long long source,long long n) 
{  long long i;
    bool vis[sz]={0}; 
    for( i=0LL;i<sz;i++) 
        dis[i]=INF; 
    class prioritize{public: bool operator ()(pair<long long,long long>&p1 ,pair<long long,long long>&p2){return p1.second>p2.second;}};
    priority_queue<pair<long long ,long long> ,vector<pair<long long,long long> >, prioritize> q; 
    q.push(make_pair(source,dis[source]=0LL));
    while(!q.empty())
    {
        pair<long long,long long> curr=q.top(); 
        q.pop();
       long long cv=curr.first,cw=curr.second; 
       if(vis[cv]) 
        continue;
        vis[cv]=true; 
        for( i=0LL;i<g[cv].size();i++)
            if(!vis[g[cv][i].first] && g[cv][i].second+cw<dis[g[cv][i].first]) 
                q.push(make_pair(g[cv][i].first,(dis[g[cv][i].first]=g[cv][i].second+cw))); 
    }
}
int main()
{   int t;
    cin>>t;
    while(t--){
    long long n,k,x,m,s,y,w,i;
    cin>>n>>k>>x>>m>>s;
    x=x*2LL ;
    for(i=1LL;i<=k;i++)
        {
         g[i].push_back(make_pair(n+1L,x/2LL)) ;
         g[n+1].push_back(make_pair(i,x/2LL)) ;
        }
        
    for( i=0LL;i<m;i++) 
    {   long long a,b;
        cin>>a>>b>>w;
        w= w * 2L;
        g[a].push_back(make_pair(b,w));
        g[b].push_back(make_pair(a,w));
    }
 
    Dijkstra(s,n+1LL);
    for( i=1L;i<=n;i++)
       cout<<dis[i]/2LL<<" ";
    cout<<endl;
     for (auto& v :g ) 
            v.clear();  
    }
    return 0;
}


