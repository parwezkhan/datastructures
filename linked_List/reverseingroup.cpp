
/*
  given a linklist reverse in a group of k 
     ex  1 2 3 4 5 6 7 8 9
         k= 3
         3 2 1 6 5 4 9 8 7

*/

#include<iostream>
#include<malloc.h>
#define null 0
using namespace std;
struct node 
{
  int data ;
  struct node *right;	
};
struct node *newnode( int key   )
{

struct node * t= (  struct node * ) malloc( sizeof(struct node ));
    t->data = key;
    t->right = null;
      return t;
}

 struct node *rev( struct node *root, int k )
 {
      if( root==null )
             return null;

      struct node *prev= null;
      struct node *current= root;
      struct node *next;
         int count=k;
            while(  count != 0  && current != null  )
             {
                  next= current->right;
                  current->right= prev ;
                  prev= current ;
                  current= next; 
                  count--;
             }  
       if( current != null )
            root->right = rev( current,k ); 
      return prev;       

 }
int main()
{
struct node *root= newnode(100);
         root->right= newnode(50);
         root->right->right= newnode(140);
         root->right->right->right=newnode(233);
         root->right->right->right->right=newnode(1234);
         root->right->right->right->right->right=newnode(1233);
         root->right->right->right->right->right->right=newnode(12334);
          int k;
         cout<<" linklist before revrese \n ";
            struct node *t= root;
              while(t != null)
              {
                  printf("%d ",t->data);
                      t=t->right;
              }	
         cout<<"   group size to wich reverse \n  ";
           cin>>k;
         root= rev( root, k );
         cout<<" linklist after reversing in group of "<< k <<endl;
             t= root;
                while( t != null )
                {
                	printf("%d ",t->data );
                	     t=t->right ;
                }
                cout<<endl;
}   
