#include<stdio.h>
#include<malloc.h>
#define null 0
struct node
        {
           int data;
           struct node *next;
        };

 struct node *newnode( int key )
 {
    struct node * t= ( struct node *)malloc( sizeof( struct node ) );
         t->data= key ;
         t->next= null;
         return t;
 }
 int main()
 {
    

    struct node * root= newnode( 100 );
         root->next= newnode( 100  );
         root->next->next= newnode(110);
         root->next->next->next= newnode( 110 );
         root->next->next->next->next=newnode( 120 );
         root->next->next->next->next->next= newnode(125);
         root->next->next->next->next->next->next = newnode(125);

         printf(" data in linklist before removing duplicates  \n");
               struct node * t= root;
                   while(  t != null )
                     {
                        printf("%d ",t->data);
                        t=t->next ;
                     }
                     printf("\n");

                t= root;
                while( t->next != null )
                  {
                     if( t->data == t->next->data )
                          t->next= t->next->next;
                      else
                          t= t->next ;  
                  }       

           printf(" data after removing duplicates \n  ");
                        t= root ;
                    while(  t != null )
                     {
                        printf("%d ",t->data);
                        t=t->next ;
                     }

return 0;

 }       