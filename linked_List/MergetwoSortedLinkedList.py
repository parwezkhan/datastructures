
"""
 Merge two linked lists
 head could be None as well for empty list
 Node is defined as
 
 class Node(object):
 
   def __init__(self, data=None, next_node=None):
       self.data = data
       self.next = next_node

 return back the head of the linked list in the below method.
"""   
def MergeLists(headA, headB):
    headC= Node()
    if headA==None and headB==None:
        return None
    elif headA==None:
        return headB
    elif headB==None:
        return headA
    elif headA.data > headB.data:
        headC.data= headB.data
        headC.next= MergeLists( headA,headB.next )
    else:
        headC.data= headA.data
        headC.next= MergeLists( headA.next,headB)
    return headC
      
      
