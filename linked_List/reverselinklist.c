#include<stdio.h>
#include<malloc.h>
#define null 0
struct node
        {
           int data;
           struct node *next;
        };

 struct node *newnode( int key )
 {
    struct node * t= ( struct node *)malloc( sizeof( struct node ) );
         t->data= key ;
         t->next= null;
         return t;
 }
 int main()
 {
    

    struct node * root= newnode( 100 );
         root->next= newnode( 105  );
         root->next->next= newnode(110);
         root->next->next->next= newnode( 115 );
         root->next->next->next->next=newnode( 120 );
         root->next->next->next->next->next= newnode(125);
         root->next->next->next->next->next->next = newnode(130);

         printf(" data in linklist before reverse  \n");
               struct node * t= root;
                   while(  t != null )
                     {
                        printf("%d ",t->data);
                        t=t->next ;
                     }
                     printf("\n");

           struct node * prev = null ;
                       
           struct node * current = root;
           struct node * nextn ;
             while( current != null )            
              {
                    nextn = current->next ;
                     current->next = prev ;
                     prev = current ;
                     current = nextn ;  
              }
                root = prev ;
                   t = root ;
           printf(" data after reversing \n  ");
                    while(  t != null )
                     {
                        printf("%d ",t->data);
                        t=t->next ;
                     }

return 0;

 }       