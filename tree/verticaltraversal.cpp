#include<iostream>
#include<malloc.h>
#include<vector> 
#include<map>
using namespace std;
#define null 0

struct node 
       {
          int data;
          struct node *left,*right;

       };
  struct node *newnode( int key )
  {

  	struct node *t=(struct node *)malloc( sizeof( struct node ));
  	     t->data=key;
  	     t->left=t->right=null;
  	     return t;
  }      
 void verticaltraversal( struct node *root, map< int, vector< int> > &m, int d)
  {
         if( root != null )
           {
                m[d].push_back( root->data );
                  verticaltraversal( root->left,m,d-1 );
                  verticaltraversal( root->right,m,d+1 );
           } 

  }
int main()
{
	struct node *root= newnode( 100 );
	    root->left=newnode(10);
	    root->right= newnode(300);
	    root->left->left=newnode(3);
	    root->left->right=newnode( 20);
	    root->right->left=newnode(150);
	    root->right->right=newnode(500);

       map< int, vector<int> > m;
         int d=0;
           verticaltraversal( root,m,d );
              map< int, vector< int> > :: iterator it;
          for(  it = m.begin(); it != m.end();it++ )
                   {
                        
                        for( int i=0; i< it->second.size();i++ )
                        	     cout<< it->second[i]<<" ";
                        	  

                        cout<<endl;	

                   }   

	return 0;
}