
/*
    height of a binary tree that is maximum height  from root to a node */



#include <iostream>
#include<malloc.h>
using namespace std;
#define null 0
struct node
        {
        int data;
        struct node *left,*right;
        };
struct node *newnode( int key )
{
   struct node *t=( struct node * )malloc( sizeof( struct node ) );
       t->data= key;
       t->left=t->right=null;
       return t;
}
int height( struct node *root )
 {
    if( root ==null )
          return 0;
     else
         return max( 1 + height( root->left),1+height(root->right ) )  ;

 }
int main()
{
    struct node *root=  newnode(10);
      root->left=newnode(12);
      root->right=newnode(30);
      root->left=newnode(44);
      root->left->right=newnode(50);
      root->right->left=newnode(111);
      root->right->right= newnode(23);
       int h= height(root);
       cout<<h<<endl;
    return 0;
}

