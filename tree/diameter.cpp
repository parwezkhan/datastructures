#include<iostream>
#include<malloc.h>
using namespace std;
#define null 0
struct node
         {
           int data ;
           struct node *left,*right;
         };
  struct node *newnode( int key  )
  {

  	struct node *t=( struct node *)malloc( sizeof( struct node  ) ) ;
  	          t->data = key ;
  	          t->left= null;
  	          t->right = null;
  	          return t;
  }       
   int d( struct node *root , int *h)
   {
      if( root == null)
      	  {
             *h=0;
             return 0;
          } 
      int lh=0;
      int rh=0;
          int ld= d( root->left,&lh);
          int rd = d( root->right,&rh);
          *h = 1 +  max( lh,rh ) ;
          return max(  rh + lh +1, max(  rd,ld ) ) ;    
   }  
 int main()
 {

  struct node *root= newnode(100);
      root->left = newnode( 50 );
      root->right= newnode( 200 );
      root->left->left= newnode( 25 );
      root->left->right = newnode( 75 );
      root->right->left= newnode(  400 );
      root->right->right= newnode( 123);
      root->right->left->left= newnode( 1233 );
      int h=0;
      cout<< d( root ,& h);
 return 0;
 }        