#include<stdio.h>
struct node
{
	int data;
	struct node *left,*right;
};
 struct node *newnode( int key )
{
    struct node *t= ( struct node * )malloc( sizeof( struct node));
        t->data= key;
        t->left=t->right=NULL;
    return t;
}

 struct node *insert( struct node *root,int key )
{
     if( root ==NULL )
         return newnode(key);
     if( root->data > key )
     	    root->left= insert( root->left,key );
     else
        root->right = insert(  root->right,key);
     return root;
}

 struct node *minnode( struct node *root )
{
    if( root->left ==NULL )
          return root;
    else
       return minnode( root->left );
}
 struct node *delet( struct node *root,int key )
{
      if(  root==NULL )
      	  return NULL;
      if( root->data > key )
            root->left= delet( root->left,key );
      else if( root->data < key )
           root->right = delet( root->right,key );
      else
          {
             if( root->left ==NULL )
             {
                 struct node *t=root->right;
                    free(root);
                    return t;
             }

             if( root->right ==NULL )
                   {

                   struct node *t=root->left;
                    free(root);
                    return t;
                   }
             struct node *t= minnode(root->right);
                root->data= t->data;
                root->right= delet(root->right,t->data);
          }
       return root;
}
 void ino( struct node *root)
  {
    if( root != NULL )
      {
          ino(root->left);
          printf("%d ",root->data);
          ino(root->right);
      }

  }
int main()
{
	int i;
        int n,j;
        struct node *root=NULL;
      printf(" enter no. of element to insert ");
         scanf("%d",&n);
     printf(" enter values in bst \n ");
              
          for(i=0;i<n;i++)
            {
                   scanf("%d",&j);
                   root=insert(root,j);

            }
            printf("\n");

        printf(" inorder traversal of bst \n ");
                   ino(root);
         printf(" enter no child node key to delete \n ");
                     scanf("%d",&j);
         root=delet(root,j);
             ino(root);
             printf(" enter one child node key to delete ");
             scanf("%d",&j);
           root=delet(root,j);
             ino(root);
             printf(" enter two child node key to delete ");
                  scanf("%d",&j);
                root= delet(root,j);
               ino(root);  
	return 0;
}
