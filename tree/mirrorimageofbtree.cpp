#include<iostream>
#include<malloc.h>
#define null 0
using namespace std;
struct node 
          {
              int data;
              struct node *left,*right;

          };
  struct node *newnode( int key )
  {
       struct node *t= (  struct node *)malloc(sizeof( struct node ));
           t->data= key;
           t->left=t->right= null;
        return t;
  }
  void inorder(  struct node *root )
  {
       if( root )
       {
       inorder(  root->left );
         cout<< root->data<<" ";
         inorder( root->right );
       }

  }        
  struct node *mirror( struct node *root )
  {
        if( root == null )
        	return null;
        struct node *t;
            t= mirror( root->left );
            root->left= mirror( root->right );
            root->right= t;
         return root;
  } 
int main()
{
struct node *root= newnode(1);
             root->left= newnode( 2 );
             root->right= newnode(3);
             root->left->left= newnode(4);
             root->left->right= newnode(5);
             root->right->left= newnode(6);
             root->right->right =newnode(7);

            cout<<"  tree before mirror image \n ";
             inorder(root);

            root= mirror( root );
             
             cout<<"  tree after coverting to mirror image \n ";
              inorder( root ); 
	return 0;
}