#include<iostream>
#include<malloc.h>
#define null 0
using namespace std;
 struct node
         {
            int data ;
            struct node *left,*right;
         };
  struct node *newnode( int key )
  {

  	struct node *t= (struct node *)malloc( sizeof(struct node ));
  	    t->data = key;
  	    t->left= t->right= null;
  	    return t;
  }  
   int search( int a[], int i,int j )
   {
      int u= -1;
       for( int k= i+1;k<=j;k++ )
       	     if( a[k] > a[i] )
       	     	    return k;

       return u; 	     
   }
  struct node* bst( int a[],int i, int j )
   {      
   	     struct node * root ;
   	     if( i > j )
   	     	 return null;
         if( j-i ==0 )
               return newnode( a[i] );
           
          else
              {
                  int k = search( a,i,j );
                  
                  if( k== -1 )
                     {
                       root = newnode( a[i] );
                          root->left = bst(  a,i+1,j );    
                          
                      }    
                  else
                  	 {
                        root= newnode( a[i] );
                        root->left= bst( a,i+1,k-1 );
                        root->right = bst(a,k,j);
                  	 }
                  
              }        
        return root ;      
   }     
  void preorder( struct node *root )
  {

      if( root )
      {

      	cout<< root->data<<" ";
      	preorder(root->left);
      	preorder(root->right);
      }


  } 
int main()
{
int n,i;
   cout<<"  enter no. of data \n ";
     cin>>n;
    int a[n];
    cout<<" enter data ";
       for(i=0;i<n;i++)
           cin>>a[i];
    struct node *root;
     root= bst( a,0,n-1);
       preorder(root);
	return 0;
}