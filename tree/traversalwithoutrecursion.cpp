#include<iostream>
#include<stack>
#include<malloc.h>
#define null 0
using namespace std;
struct node 
       {
          int data;
          struct node *left,*right;

       };
struct node *newnode(int key )
{
    struct node * t= (struct node *)malloc(sizeof(struct node ));
          t->data=key;
          t->left=t->right=null;
          return t;
} 
void inorder(  struct node * root )
{
   stack< struct node *> s;

    struct node *current= root;
         s.push(  current );
         current=current->left;
       while(  current != null  ||  !( s.empty()) )
       {
            while( current != null )
            	{    s.push( current) ;

                     current= current->left ;
                }

                current= s.top();
                s.pop();
                  printf("%d ",current->data);
                current= current->right;  
       }
}
/*void preorder( struct node *root)
{
    stack< struct node *>s;
    struct node *current;
        s.push( root );
      while( ! s.empty() )
        {
            current= s.top();
               printf("%d ",current->data);
                
                                 
                  
             
                 

        }

}  */
int main()
{
	
    struct node *root= newnode(23);
    root->left=newnode(18);
    root->right=newnode(100);
    root->left->left=newnode(10);
    root->left->right=newnode(21);
    root->right->left=newnode(50);
    root->right->right=newnode(200);
    inorder(root); 
	return 0;
}
