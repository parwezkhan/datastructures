
/*
    level order traversal of binary tree  or in same horizonatl line
*/


#include <iostream>
#include<queue>
#include<malloc.h>
using namespace std;
#define null 0
struct node
        {
        int data;
        struct node *left,*right;
        };
struct node *newnode( int key )
{
   struct node *t=( struct node * )malloc( sizeof( struct node ) );
       t->data= key;
       t->left=t->right=null;
       return t;
}
void levelTraversal( struct node *root)
{

queue< struct node *>q;
 q.push( root );
      while( ! q.empty() )
       {

           int l= q.size();
            while(l--)
             {
                  struct node *t= q.front();
                     cout<<t->data<<" ";
                        q.pop();
                    if( t->left != null )
                        q.push( t->left );
                    if( t->right != null )
                         q.push( t->right );

             }
           cout<<endl;
       }

}
int main()
{
    struct node *root=  newnode(10);
      root->left=newnode(12);
      root->right=newnode(30);
      root->left->left=newnode(44);
      root->left->right=newnode(50);
      root->right->left=newnode(111);
      root->right->right= newnode(23);
       levelTraversal(root);
    return 0;
}

