

/*
   construction of tree using preorder and inorder of a tree

*/

#include<iostream>
#include<malloc.h>
using namespace std;
#define null 0
struct node
          {
            int data ;
            struct node *left,*right;
          };
 struct node *newnode( int key )
 {
        struct node *t=( struct node * )malloc( sizeof( struct node ));
             t->data = key;
             t->left=t->right= null;
        return t;
 }    

   int search( int in[],int x, int n )
    {
          for (int i = 0; i < n; ++i)
          {
          	  if( in[i] == x )
          	  	   return i;

          }
          return -1;

    }
  struct node *tree( int in[],int pre[], int n )
    {
        
      struct node *root = ( struct node * ) malloc( sizeof( struct node )) ;  
      root->data = pre[0] ;
      int j= search( in,pre[0],n );
      if( j!= 0 )
      	  root->left = tree( in,pre+1, j ) ;
      else
          root->left = null;  
      if( j!= n-1  )
           root->right = tree( in+ j+1,pre +j+1, n-j-1 ) ;
      else
          root->right = null;   
         return root;
    }
  void post( struct node *root )
  {
     if( root != null )
     {
         post( root->left );
         post( root->right );
            cout<< root->data <<" ";
     }
  }      
 int main( )
 {
     int n,i;
     cout<< " enter no. of data in tree \n ";
     cin>>n;
     int pre[n];
     int in[n];
     cout<< "  enter preorder of tree \n";
        for( i=0;i<n;i++)
        	  cin>>pre[i];
        cout<< "  enter inorder of tree \n  ";
          for( i=0;i<n;i++)
                cin>>in[i];
     struct node *root;
         root = tree( in, pre, n );
      
      cout<< " post order of constructed tree to test tree  \n ";
          post( root ) ;

    return 0;
 }
