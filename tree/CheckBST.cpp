
//given root of a tree check if this is BST or not 


#define null 0
Node* prev;
   bool checkBST(Node* root) {
     if(root)
     {
         if( !checkBST(root->left) )
         {   
             return false;   
         }
         if( prev != null  and prev->data >= root->data )
               return false;
         prev=root;
         return checkBST( root->right );
     }  
       return true;
   }

