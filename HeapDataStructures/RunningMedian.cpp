/*

 coming a stream of numbers finding median so far 
  

    algorithms
     1. make one max heap and one min heap
          for the first two elements put larger in min heap and smaller in max heap
     2.  after getting a new element if this is smaller than root of max heap put in max heap
         else  in min heap
     3. Now ballance if difference between no. of elements in two heaps is 2 
           take root of heap containing more elements  put in other heap
    4.  median calculation
             if no. of elements in both equal
                    median = (  root of max heap + root of min heap)/2
             else
                 root of heap containg  more no. of elements 
   time   Complexity:  order O(log(n))

*/
#include<bits/stdc++.h>
using namespace std;
bool cmp(int x,int y)   //  comarator for min heap
{
  return x > y;
}
int main() {
     int n,i,j;
     cin>>n;
     if( n ==1 )
     {  cin>>i;cout<<i;      }
     else if( n==2)
     {
        cin>>i;  cout<<i<<endl;
        cin>>j;
       cout<<( float ) ( i+j)/2<<endl;
     }
     else
     {
        vector< int> v1; // maxheap
        vector< int>v2;// minheap
        cin>>i;
        cout<<i<<endl;
       cin>>j;
        cout<<(float) ( i+j )/2 <<endl;
           if( i > j )
            swap(i,j); 
           v1.push_back(i); 
           v2.push_back(j);
          make_heap( v1.begin(),v1.end() );  
          make_heap(v2.begin(),v2.end(),cmp);
            for(i=0;i<(n-2);i++)
            {   cin>>j;
                if( j < v1.front()  )
                {
                     v1.push_back( j );
                     push_heap( v1.begin(),v1.end());
                }
                else
                {
                     v2.push_back( j );
                     push_heap( v2.begin(),v2.end() ,cmp);
                }
                 int   m1= v1.size();
                 int   m2 = v2.size();
                if(  (  m1 - m2  ) >1 ) 
                {    
                      j= v1.front();
                      pop_heap(v1.begin(),v1.end());
                      v1.pop_back();
                      v2.push_back( j );
                      push_heap( v2.begin(),v2.end() ,cmp);    
                }
               else if( ( m2 - m1 ) >1  )
                {   
                      j= v2.front();
                      pop_heap(v2.begin(),v2.end(),cmp);
                       v2.pop_back();
                       v1.push_back( j );
                      push_heap( v1.begin(),v1.end());    
                }
                if( v1.size() > v2.size() )
                 cout<<v1.front()<<endl;
             else if( v2.size() > v1.size() )
                 cout<<v2.front()<<endl;
             else
                 cout<< ( float ) ( v1.front() + v2.front())/2 <<endl;
             
            }
            
     }
    return 0;
}
