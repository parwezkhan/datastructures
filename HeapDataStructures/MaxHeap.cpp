#include<bits/stdc++.h>
using namespace std;
class MaxHeap
{
   vector<int> v;	

  public:
  int getParent(int i)
    {
        return ( i-1)/2 ;
    }	
  int getLeftChild(int i)
      {
      	return 2*i +1;
      }
   int getRightChild(int i)
     {
     return 2*i +2;
     }
   void heapify_down( int i )
      {
          int largest=i;
          int l= getLeftChild(i);
          int r= getRightChild(i);
          if( l< v.size() &&  v[largest] < v[l]  )
          	        largest= l;
          if(  r< v.size()  && v[largest] < v[r] )	 
                   largest= r;
          if( largest != i )
            {
                 swap( v[largest],v[i] ) ;
                 heapify_down( largest );
            }            

      }
      void heapify_up(int i)
       {
           if(  v[i] > v[ getParent(i)] )
           	   {
                    swap( v[i], v[getParent(i)] );
                     heapify_up( getParent(i) );

           	   }

       }
     void push(int u )
       {
            v.push_back( u );
            if( v.size() > 1 );
            heapify_up( v.size()-1 );
       }  
     void pop()
      {
           if( v.size() != 0 )
           	 {
                   v[0]= v[ v.size()-1 ];
                   v.pop_back();
                   if( v.size() >1 )
                       heapify_down(0) ;

           	 }

      }  
      int top()
       {
            if( v.size() >0 )
            	return v[0] ;
     
       }

};
int main()
{
   
 int i,j;
  MaxHeap hp;
  hp.push(23);
  hp.push(25);
  hp.push(10);  hp.push(100);  hp.push( 234); hp.push(14);
       cout<<hp.top()<<endl;  
       hp.pop();  cout<<hp.top()<<endl;
       hp.pop();cout<<hp.top()<<endl;
       hp.pop();cout<<hp.top()<<endl;
       hp.pop();cout<<hp.top()<<endl;
       hp.pop();cout<<hp.top()<<endl;
       hp.pop();cout<<hp.top()<<endl;
	
	return 0;
}
