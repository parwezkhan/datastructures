

#include<bits/stdc++.h>
using namespace std;
bool cmp( int x, int y )
{
    return x > y;
}
int main()
{
   vector<int>v1;
   vector<int>v2;
   v1.push_back(4);v1.push_back(10);v1.push_back(50);v1.push_back(70);v1.push_back(23);
   v2.push_back(4);v2.push_back(10);v2.push_back(14);v2.push_back(123);v2.push_back(101);
     // conversion of vector to heap
    make_heap(v1.begin(),v1.end());
    make_heap(v2.begin(),v2.end(),cmp);
      cout<<" top element of maxheap "<<v1.front()<<endl;
      cout<<"  top element of minheap "<<v2.front()<<endl;
    
    //  to push a value in max heap
          v1.push_back(100);
         // now heapify
           push_heap(v1.begin(),v1.end());
                cout<<" after pushing top element of heap "<< v1.front() <<endl;
          // to push a value in min heap
              v2.push_back(3);
    // now heapify
          push_heap(v2.begin(),v2.end(),cmp);
            cout<<" after pushing top element in min heap "<<v2.front()<<endl;
          // to pop in max heap
           pop_heap(v1.begin(),v1.end()); // it puts root element to last and heapify but size does not decrease
            //  now decreasing size
                v1.pop_back();
            cout<<" top element after pop  "<<v1.front()<<endl;
      // to pop min heap
             pop_heap(v2.begin(),v2.end(),cmp); // it puts root element to last and heapify but size does not decrease
           // now decreasing
              v2.pop_back();
          cout<<" top element after pop "<<v2.front()<<endl;
          
          // to sort element   now  container is no longer a heap
             sort_heap(v1.begin(),v1.end()); // in asending order
           cout<<" after sorting elements are  ";
                  for( int x : v1 )
                       cout<<x<<" ";
           cout<<endl;
          // to sort min heap  Now container is no longer a heap
               sort_heap(v2.begin(),v2.end(),cmp); // in desending order
              cout<<" after sorting elements are ";
                    for(  int x : v2 )
                         cout<<x<<" ";
    
    return 0;
}    
