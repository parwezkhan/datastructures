
/*
   mean heap Implementation and deletion of any value
  all values are unique

   Input format 
      q   // no of queries
      // then q lines as
      1 v  // push v in heap
      2 v  // delete v from heap
      3  // dispaly top element     

*/



#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
class MinHeap
{
   vector<int> v;	
  public:
  int getParent(int i)
    {
        return ( i-1)/2 ;
    }	
  int getLeftChild(int i)
      {
      	return 2*i +1;
      }
   int getRightChild(int i)
     {
     return 2*i +2;
     }
   void heapify_down( int i )
      {
          int smallest=i;
          int l= getLeftChild(i);
          int r= getRightChild(i);
          if( l< v.size() &&  v[smallest] > v[l]  )
          	        smallest= l;
          if(  r< v.size()  && v[smallest] > v[r] )	 
                   smallest= r;
          if( smallest != i )
            {
                 swap( v[smallest],v[i] ) ;
                 heapify_down( smallest );
            }            

      }
      void heapify_up(int i)
       {
           if(  v[i] <  v[ getParent(i)] )
           	   {
                    swap( v[i], v[getParent(i)] );
                     heapify_up( getParent(i) );

           	   }
       }
     void push(int u )
       {
            v.push_back( u );
            if( v.size() > 1 );
            heapify_up( v.size()-1 );
       }  
    void delet( int u )
      {  int i;
          for( i=0;i< v.size();i++ )
               {
                   if( v[i]== u  )
                       break;
               }
           v[i] = v[ v.size() -1 ];
           v.pop_back();
              if( i!= ( v.size() -1 ) )
                   heapify_down(i) ;
        }  
      int top()
       {
            	return v[0] ;
       }
      
};
bool cmp(int x, int y)
{
    
    return x > y;
}   
int main() {
        MinHeap h;
        int q,i,j;
        cin>>q;
        while(q--)
        {
            cin>>i;
            if(i==1)
            {  cin>>j;
               h.push(j);
            }
            else if( i==2 )
            {  cin>>j;
               h.delet(j);
            }  
            else
             cout<<h.top()<<endl;
        }    
     
    return 0;
}

