
/*

   disjoint set implementations 
   union by rank 


*/

#include <iostream>
using namespace std;
int *p;
int *r;
int find_set( int x )  //  find set representative containing element x
{
    if( x != p[x ] )
         p[x]= find_set( p[ x ] );
    return p[x];
}
void Create_Set(int x) // to create a new set containing x 
{
    p[x]= x;  // parent of x
    r[x]=0;
}
void merge_set(int x,int y ) // merging of two sets  containing x, containing y
{
    int px = find_set(x ); // set representative  containing x
    int py = find_set(y);
    if( r[ px ] > r[py] )
          p[ py ]= px;
    else
        p[ px ]= py;
    if( r[ px ] == r[ py ] )
          r[ py ]+=1;
}
bool Issame_set( x,y  ) //  to check x, y are in same set 
   {
       return find_set( x ) == find_set( y );
   }
int main()
{
    int n;
    cout<<" enter no. of elements in set "<<endl;
    cin>>n;
    p= new int[n+1];
    r= new int[n+1];
    for(  int i=1;i< n+1;i++)
         Create_Set(i );


    return 0;
}
   
