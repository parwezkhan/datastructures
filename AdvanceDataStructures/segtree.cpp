#include<bits/stdc++.h>
using namespace std;
int *a;
int *tree;
int *lazy; 
void build(int node,int s,int e)
{
  if(s==e)
  {
    tree[node]=a[s];
  }
  else
  {   int mid= s+ (e-s)/2 ;
      build(2*node+1,s,mid);  build(2*node+2,mid+1,e);
      tree[node]=tree[2*node+1]+tree[2*node+2];
  }
}
void update(int node,int s,int e,int l,int r,int v)
  {
       if(lazy[node]!=0)
         {
           tree[node] += (e-s+1)* lazy[node] ;
             if(s != e)
               {
                 lazy[2*node+1]+=lazy[node];
                 lazy[2*node+2]+=lazy[node];
               }
            lazy[node]=0;
         }
      if(s>e || s>r || e<l)
          return;
      else if( s >= l && e<= r)
          {
                tree[node]+=(e-s+1)*v;
                 if(s!=e)
                 {
                     lazy[node*2+1]+=v;
                     lazy[node*2+2]+=v;
                 }
              return;
          }
              int  mid = s+(e-s)/2;
              update(node*2+1,s,mid,l,r,v);
              update(node*2+2,mid +1,e,l,r,v);
              tree[node]=tree[2*node+1]+tree[2*node+2];     
  }
 int getsum(int node,int s,int e,int l,int r)
  {
     if( s>e || s>r || e<l )
       return 0;        
      if(lazy[node]!=0)
         {
           tree[node]+=(e-s+1)*(lazy[node]);
               if(s!=e)
                 {
                   lazy[node*2+1]=lazy[node];
                   lazy[node*2+2]=lazy[node];
                 }
             lazy[node]=0;
         }
      if( s>=l && r>= e)
         return tree[node];
      int mid=s+(e-s)/2 ;
      return getsum( node*2+1,s,mid,l,r) + getsum(node*2+2,mid+1,e,l,r);
  }
int main()
{
 int n,i,q,l,r,v,sum=0;
 cin>>n>>q;
  a=new int[n];
 tree= new int[2*n];
 lazy= new int[2*n]; 
 for(i=0;i<n;i++)
  cin>>a[i];
build(0,0,n-1);
 for(i=0;i< 2*n;i++)
   lazy[i]=0;
 while(q--)
  {
    int op;
    cin>>op;
    if(op==1)
    { cin>>l>>r;
       sum = getsum(0,0,n-1,l,r);
       cout<<sum<<endl;
    }
    else
     {   cin>>l>>r>>v;
         update(0,0,n-1,l,r,v);
     }
  }
  return 0;
}