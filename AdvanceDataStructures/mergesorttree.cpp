/*
given t test cases  in each test first line  case n q  no. of elemnts in an list and no. of querries to be perfofmed
second line list  then followed  q line each l r k
find no. of elemnts between l to r both inclusive greater than equal to k
t
n q
a0 a1 a2    ....   an-1
l r k
note indexing stat from 1
                      */
#include<bits/stdc++.h>
using namespace std;
long n;
vector<long>tree[100000];   // array of vectors
void build_tree(vector<long> & a,long cur ,long l ,long r )
{
     if( l==r )
     { tree[cur].push_back( a[ l ] ); return ; }
     long mid = l+(r-l)/2;
     build_tree(a,2*cur, l , mid );
     build_tree(a,2*cur+1, mid+1 , r );
     long le= cur*2 , ri= cur*2 +1 ;
     merge(tree[le].begin(),tree[le].end(),tree[ri].begin(),tree[ri].end(),back_inserter(tree[cur])); 
}
long query(long cur,long l,long r,long x,long y,long k)
{
       if( r < x || l > y )
      {
               return 0; 
      }
      if( x<=l && r<=y )
      {    
           long u= lower_bound(tree[cur].begin(),tree[cur].end(),k) - tree[cur].begin() ;
           return  tree[cur].size() -u ;
      }
     long mid=l+(r-l)/2L;
     return query(2*cur,l,mid,x,y,k)+query(2*cur+1,mid+1,r,x,y,k);
}   
int main()
{
    int t;
    cin>>t;
    while(t--)
     {
      long q,i,j,l,r,k;
      cin>>n>>q;
      vector<long>v;
      v.push_back(-2);  // just to start indexing from 1
      for(i=0L;i<n;i++)
         {  cin>>j;
            v.push_back(j);
         }
        build_tree(v,1,1,v.size()-1) ;
        while(q--)
         {    cin>>l>>r>>k;
             cout<<query(1,1,v.size()-1,l,r,k)<<endl;
         }      
         for (auto& v :tree) 
             v.clear();  
     }    
    return 0;
}  
